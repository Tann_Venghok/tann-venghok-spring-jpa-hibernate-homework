//package com.homeworkspringjpahibernate.demo.config;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.client.RestTemplate;
//import org.codehaus.jackson.map.ObjectMapper;
//import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
//
//@Configuration
//    @ComponentScan("com.homeworkspringjpahibernate.demo")
//    public class AppConfig {
//
//        @Bean
//        RestTemplate restTemplate() {
//            RestTemplate restTemplate = new RestTemplate();
//            MappingJacksonHttpMessageConverter converter = new MappingJacksonHttpMessageConverter();
//            converter.setObjectMapper(new ObjectMapper());
//            restTemplate.getMessageConverters().add(converter);
//            return restTemplate;
//        }
//    }
