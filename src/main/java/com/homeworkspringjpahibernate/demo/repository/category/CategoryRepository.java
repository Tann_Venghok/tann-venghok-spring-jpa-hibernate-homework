package com.homeworkspringjpahibernate.demo.repository.category;

import com.homeworkspringjpahibernate.demo.model.Category;

public interface CategoryRepository {
    public void saveCategory(Category category);
}
