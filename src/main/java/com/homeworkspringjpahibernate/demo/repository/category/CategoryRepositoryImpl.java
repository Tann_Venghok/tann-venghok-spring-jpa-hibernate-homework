package com.homeworkspringjpahibernate.demo.repository.category;

import com.homeworkspringjpahibernate.demo.model.Category;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional
public class CategoryRepositoryImpl implements CategoryRepository{
    @PersistenceContext
    private EntityManager em;

    @Override
    public void saveCategory(Category category) {
        em.persist(category);
    }

    public List<Category> getAllCategory() {
        return em.createQuery("Select c from Category c").getResultList();
    }

    public Category getOneCategory(int id) {
        return (Category) em.createQuery("Select c from Category c Where c.id = :id")
                .setParameter("id", id)
                .getSingleResult();
    }


    public int deleteCategory(int id) {
        return em.createQuery("Delete from Category c WHERE c.id = :id ")
                .setParameter("id", id).executeUpdate();
    }

    public int updateCategory(int id, Category category) {
        return em.createQuery("Update Category c SET c.title = :title WHERE c.id =:id")
                .setParameter("id", id)
                .setParameter("title", category.getTitle())
                .executeUpdate();
    }
}
