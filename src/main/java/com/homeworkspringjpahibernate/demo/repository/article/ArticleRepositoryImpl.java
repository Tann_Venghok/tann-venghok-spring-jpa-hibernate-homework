package com.homeworkspringjpahibernate.demo.repository.article;

import com.homeworkspringjpahibernate.demo.model.Article;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
@Transactional
public class ArticleRepositoryImpl implements ArticleRepository {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List getAllArticle() {
        Query q = entityManager.createQuery("Select a from Article a");
        return q.getResultList();
    }

    @Override
    public List<Article> getByCategoryTitle(String title) {
        Query query = entityManager.createQuery("Select a From Article a JOIN FETCH a.category c Where c.title = :title")
                .setParameter("title", title);
        return (List<Article>) query.getResultList();
    }

    @Override
    public Article getOneArticle(int id) {
        Query query = entityManager.createQuery("Select a from Article a Where a.id = :id")
                .setParameter("id", id);
        return (Article) query.getSingleResult();
    }

    @Override
    public int updateArticle(int id, Article article) {
        Query query = entityManager.createQuery("Update Article a SET a.author = :author, a.description = :description," +
                " a.title = :title, a.category= :categoryId WHERE a.id =:id")
                .setParameter("id", id)
                .setParameter("author", article.getAuthor())
                .setParameter("description", article.getDescription())
                .setParameter("title", article.getTitle())
                .setParameter("categoryId", article.getCategory());
        return query.executeUpdate();
    }

    @Override
    public void deleteArticle(int id) {
        Query query = entityManager.createQuery("Delete from Article a WHERE a.id = :id ")
                .setParameter("id", id);
        query.executeUpdate();
    }

    @Transactional
    @Override
    public void saveBook(Article article) {
//        Article a = new Article("Kaka", "kaka from the hood", "hey yo", new Category(1,"novel"));
//        Article b = new Article("Koko", "kaka from the hood", "hey yo", new Category(2,"horror"));
//        Article c = new Article("Kiki", "kaka from the hood", "hey yo", new Category(3,"documentary"));
//        entityManager.persist(a);
//        entityManager.persist(b);
//        entityManager.persist(c);

        entityManager.persist(article);
    }
}
