package com.homeworkspringjpahibernate.demo.service.category_service;

import com.homeworkspringjpahibernate.demo.model.Article;
import com.homeworkspringjpahibernate.demo.model.Category;

import java.util.List;

public interface CategoryService {
    public void saveCategory(Category category);

    List<Category> getAllCategory();

    Category getOneCategory(int id);

    int deleteCategory(int id);

    int updateCategory(int id, Category category);
}
