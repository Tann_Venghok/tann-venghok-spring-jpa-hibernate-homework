package com.homeworkspringjpahibernate.demo.service.category_service;

import com.homeworkspringjpahibernate.demo.model.Article;
import com.homeworkspringjpahibernate.demo.model.Category;
import com.homeworkspringjpahibernate.demo.repository.category.CategoryRepository;
import com.homeworkspringjpahibernate.demo.repository.category.CategoryRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService{
    @Autowired
    private CategoryRepositoryImpl categoryRepository;

    @Override
    public void saveCategory(Category category) {
        categoryRepository.saveCategory(category);
    }

    @Override
    public List<Category> getAllCategory() {
        return categoryRepository.getAllCategory();
    }

    @Override
    public Category getOneCategory(int id) {
        return categoryRepository.getOneCategory(id);
    }

    @Override
    public int deleteCategory(int id) {
        return categoryRepository.deleteCategory(id);
    }

    @Override
    public int updateCategory(int id, Category category) {
        return categoryRepository.updateCategory(id, category);
    }
}
