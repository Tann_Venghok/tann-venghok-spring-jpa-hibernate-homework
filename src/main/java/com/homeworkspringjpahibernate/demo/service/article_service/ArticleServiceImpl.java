package com.homeworkspringjpahibernate.demo.service.article_service;

import com.homeworkspringjpahibernate.demo.model.Article;
import com.homeworkspringjpahibernate.demo.repository.article.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImpl implements ArticleService {
    @Autowired
    private ArticleRepository articleRepository;
    @Override
    public List<Article> getAllArticle() {
        return articleRepository.getAllArticle();
    }

    @Override
    public List<Article> getByCategoryTitle(String title) {
        return articleRepository.getByCategoryTitle(title);
    }

    @Override
    public Article getOneArticle(int id) {
        return articleRepository.getOneArticle(id);
    }

    @Override
    public int updateArticle(int id, Article article) {
        return articleRepository.updateArticle(id, article);
    }

    @Override
    public void deleteArticle(int id) {
        articleRepository.deleteArticle(id);
    }

    @Override
    public void saveBook(Article article) {
        articleRepository.saveBook(article);
    }
}
