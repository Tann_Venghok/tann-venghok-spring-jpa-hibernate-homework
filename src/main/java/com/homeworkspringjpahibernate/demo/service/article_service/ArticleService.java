package com.homeworkspringjpahibernate.demo.service.article_service;

import com.homeworkspringjpahibernate.demo.model.Article;

import java.util.List;

public interface ArticleService {
    public List<Article> getAllArticle();
    public List<Article> getByCategoryTitle(String title);
    public Article getOneArticle(int id);
    public int updateArticle(int id, Article article);
    public void deleteArticle(int id);
    public void saveBook(Article article);
}
