package com.homeworkspringjpahibernate.demo.controller;

import com.homeworkspringjpahibernate.demo.controller.response.BaseApiResponse;
import com.homeworkspringjpahibernate.demo.model.Article;
import com.homeworkspringjpahibernate.demo.model.Category;
import com.homeworkspringjpahibernate.demo.service.category_service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    @PostMapping()
    public BaseApiResponse<Category> insertCategory(@RequestBody Category category){
        BaseApiResponse<Category> baseApiResponse = new BaseApiResponse<>();

        try{
            categoryService.saveCategory(category);
            baseApiResponse.setMessage("Category Insert Successfully");
            baseApiResponse.setStatus(HttpStatus.OK);
            baseApiResponse.setSuccess(true);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        }catch (Exception e){
            baseApiResponse.setMessage("Failed, Cannot Save Data");
            baseApiResponse.setStatus(HttpStatus.BAD_REQUEST);
            baseApiResponse.setSuccess(false);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        }
        return baseApiResponse;
    }


    @GetMapping()
    public BaseApiResponse<List<Category>> getAllCategory(){
        BaseApiResponse<List<Category>> baseApiResponse = new BaseApiResponse<>();

        try{
            baseApiResponse.setData(categoryService.getAllCategory());
            baseApiResponse.setMessage("Retrieved Data Successfully");
            baseApiResponse.setStatus(HttpStatus.OK);
            baseApiResponse.setSuccess(true);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        }catch (Exception e){
            baseApiResponse.setMessage("Error Occurred");
            baseApiResponse.setStatus(HttpStatus.BAD_REQUEST);
            baseApiResponse.setSuccess(false);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        }
        return baseApiResponse;
    }

    @GetMapping("/{id}")
    public BaseApiResponse<Category> getOneCategoryById(@PathVariable("id") int id){
        BaseApiResponse<Category> baseApiResponse = new BaseApiResponse<>();

        try{
            baseApiResponse.setData(categoryService.getOneCategory(id));
            baseApiResponse.setMessage("Retrieved Data Successfully");
            baseApiResponse.setStatus(HttpStatus.OK);
            baseApiResponse.setSuccess(true);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        }catch (Exception e){
            baseApiResponse.setMessage("Error Occurred");
            baseApiResponse.setStatus(HttpStatus.BAD_REQUEST);
            baseApiResponse.setSuccess(false);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        }
        return baseApiResponse;
    }

    @DeleteMapping("/{id}")
    public BaseApiResponse<Category> deleteCategory(@PathVariable("id") int id){
        BaseApiResponse<Category> baseApiResponse = new BaseApiResponse<>();

        int i = categoryService.deleteCategory(id);
        if(i == 1){
            baseApiResponse.setMessage("Data Deleted Successfully");
            baseApiResponse.setStatus(HttpStatus.OK);
            baseApiResponse.setSuccess(true);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        }else {
            baseApiResponse.setMessage("Failed, Cannot Delete Data");
            baseApiResponse.setStatus(HttpStatus.BAD_REQUEST);
            baseApiResponse.setSuccess(false);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        }
        return baseApiResponse;
    }

    @PutMapping("/{id}")
    public BaseApiResponse<Category> updateCategory(@PathVariable("id") int id, @RequestBody Category category){
        BaseApiResponse<Category> baseApiResponse = new BaseApiResponse<>();

        int i = categoryService.updateCategory(id, category);
        if(i == 1){
            baseApiResponse.setMessage("Data Updated Successfully");
            baseApiResponse.setStatus(HttpStatus.OK);
            baseApiResponse.setSuccess(true);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        }else {
            baseApiResponse.setMessage("Failed, Cannot Update Data");
            baseApiResponse.setStatus(HttpStatus.BAD_REQUEST);
            baseApiResponse.setSuccess(false);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        }
        return baseApiResponse;
    }
}
