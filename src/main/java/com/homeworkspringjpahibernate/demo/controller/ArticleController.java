package com.homeworkspringjpahibernate.demo.controller;

import com.homeworkspringjpahibernate.demo.controller.response.BaseApiResponse;
import com.homeworkspringjpahibernate.demo.model.Article;
import com.homeworkspringjpahibernate.demo.model.Category;
import com.homeworkspringjpahibernate.demo.service.article_service.ArticleService;
import com.homeworkspringjpahibernate.demo.service.category_service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/article")
public class ArticleController {
    @Autowired
    private ArticleService articleService;
    @Autowired
    private CategoryService categoryService;

    @GetMapping()
    public BaseApiResponse<List<Article>> getAllArticle(){
        BaseApiResponse<List<Article>> baseApiResponse = new BaseApiResponse<>();
        
        try{
            baseApiResponse.setData(articleService.getAllArticle());
            baseApiResponse.setMessage("Retrieved Data Successfully");
            baseApiResponse.setStatus(HttpStatus.OK);
            baseApiResponse.setSuccess(true);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        }catch (Exception e){
            baseApiResponse.setMessage("Error Occurred");
            baseApiResponse.setStatus(HttpStatus.BAD_REQUEST);
            baseApiResponse.setSuccess(false);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        }
        return baseApiResponse;
    }

    @GetMapping("/")
    public BaseApiResponse<List<Article>> getByCategoryTitle(@RequestParam("title") String title){
        BaseApiResponse<List<Article>> baseApiResponse = new BaseApiResponse<>();

        try{
            baseApiResponse.setData(articleService.getByCategoryTitle(title));
            baseApiResponse.setMessage("Retrieved Data Successfully");
            baseApiResponse.setStatus(HttpStatus.OK);
            baseApiResponse.setSuccess(true);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        }catch (Exception e){
            baseApiResponse.setMessage("Error Occurred");
            baseApiResponse.setStatus(HttpStatus.BAD_REQUEST);
            baseApiResponse.setSuccess(false);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        }
        return baseApiResponse;
    }

    @GetMapping("/{articleid}")
    public BaseApiResponse<Article> getArticleById(@PathVariable("articleid") int id){
        BaseApiResponse<Article> baseApiResponse = new BaseApiResponse<>();

        try{
            baseApiResponse.setData(articleService.getOneArticle(id));
            baseApiResponse.setMessage("Retrieved Data Successfully");
            baseApiResponse.setStatus(HttpStatus.OK);
            baseApiResponse.setSuccess(true);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        }catch (Exception e){
            baseApiResponse.setMessage("Error Occurred");
            baseApiResponse.setStatus(HttpStatus.BAD_REQUEST);
            baseApiResponse.setSuccess(false);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        }
        return baseApiResponse;
    }

    @DeleteMapping("/{id}")
    public BaseApiResponse<Article> deleteArticle(@PathVariable("id") int id){
//        articleService.deleteArticle(id);
        BaseApiResponse<Article> baseApiResponse = new BaseApiResponse<>();

        try{
            articleService.deleteArticle(id);
            baseApiResponse.setMessage("Data Deleted Successfully");
            baseApiResponse.setStatus(HttpStatus.OK);
            baseApiResponse.setSuccess(true);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        }catch (Exception e){
            baseApiResponse.setMessage("Failed, Cannot Delete Data");
            baseApiResponse.setStatus(HttpStatus.BAD_REQUEST);
            baseApiResponse.setSuccess(false);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        }
        return baseApiResponse;
    }

    @PutMapping("/{id}")
    public BaseApiResponse<Article> updateArticle(@PathVariable("id") int id, @RequestBody Article article){
        BaseApiResponse<Article> baseApiResponse = new BaseApiResponse<>();

        int i = articleService.updateArticle(id, article);;
        if(i == 1){
            baseApiResponse.setMessage("Data Updated Successfully");
            baseApiResponse.setStatus(HttpStatus.OK);
            baseApiResponse.setSuccess(true);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        }else {
            baseApiResponse.setMessage("Failed, Cannot Update Data");
            baseApiResponse.setStatus(HttpStatus.BAD_REQUEST);
            baseApiResponse.setSuccess(false);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        }
        return baseApiResponse;
    }

    @PostMapping()
    public BaseApiResponse<Article> insertArticle(@RequestBody Article article){

        BaseApiResponse<Article> baseApiResponse = new BaseApiResponse<>();

        try{
            articleService.saveBook(article);
            baseApiResponse.setMessage("Article Insert Successfully");
            baseApiResponse.setStatus(HttpStatus.OK);
            baseApiResponse.setSuccess(true);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        }catch (Exception e){
            baseApiResponse.setMessage("Failed, Cannot Insert Data");
            baseApiResponse.setStatus(HttpStatus.BAD_REQUEST);
            baseApiResponse.setSuccess(false);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        }
        return baseApiResponse;
    }

}
