package com.homeworkspringjpahibernate.demo.controller.resttemplate;

import com.homeworkspringjpahibernate.demo.model.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@RestController
public class ArticleRestTemplate {
    @Autowired
    RestTemplate restTemplate;

    @GetMapping(value = "/restTemplate")
    public String getAllArticles(){
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept((Arrays.asList(MediaType.APPLICATION_JSON)));
        HttpEntity<Article> entity = new HttpEntity<Article>(headers);

        return restTemplate.exchange(
                "http://localhost:8080/article", HttpMethod.GET, entity, String.class).getBody();
    }

    @GetMapping(value = "/restTemplate/{id}")
    public String getArticleById(@PathVariable("id") int id){
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept((Arrays.asList(MediaType.APPLICATION_JSON)));
        HttpEntity<Article> entity = new HttpEntity<>(headers);
        return  restTemplate.exchange("http://localhost:8080/article/" + id, HttpMethod.GET, entity,
                String.class).getBody();
    }

    @GetMapping(value = "/restTemplate/")
    public ResponseEntity<String> getArticleById(@RequestParam("title") String title){
        Map< String, String> params = new HashMap<>();
        params.put("title",title);
         return
                restTemplate.getForEntity("http://localhost:8080/article/?title={title}",String.class,params);
    }

    @RequestMapping(value = "/restTemplate", method = RequestMethod.POST)
    public String createArticle(@RequestBody Article article) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<Article> entity = new HttpEntity<Article>(article,headers);

        return restTemplate.exchange(
                "http://localhost:8080/article", HttpMethod.POST, entity, String.class).getBody();
    }

    @RequestMapping(value = "/restTemplate/{id}", method = RequestMethod.PUT)
    public void updateArticle(@PathVariable("id") Integer id, @RequestBody Article article) {
        restTemplate.put("http://localhost:8080/article/" + id.toString(),
                article,
                Integer.toString(article.getId()));
    }

    @RequestMapping(value = "/restTemplate/{id}", method = RequestMethod.DELETE)
    public void deleteArticle(@PathVariable("id") Integer id) {
        restTemplate.delete("http://localhost:8080/article/" + id.toString());
    }
}
