CREATE TABLE tb_category(
                            id INT  PRIMARY  KEY auto_increment,
                            title varchar not null
);
CREATE TABLE tb_articles(
                        id INT  PRIMARY  KEY auto_increment,
                        author varchar,
                        description varchar,
                        title varchar not null,
                        category_id int not null,
                        FOREIGN KEY (category_id) REFERENCES tb_category(id)
);
